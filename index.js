// Define a Neode Instance
const neode = require("neode")

// Using configuration from .env file
    .fromEnv()
    .withDirectory(__dirname + '/mdoels');

neode.setEnterprise(true);

/*
 .with({
        Person: require("./models/Person"),
        Project: require("./models/project"),
    });
 */


