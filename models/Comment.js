module.exports = {
    "id": {
        type: "number",
        primary: true
    },

    "description": {
        type: "string",
        default: "Issue description"
    },

    "created_at": {
        type: "date",
        default: () => new Date()
    },

    "creator_id": {
        type: "integer"
    },
    "issues_id": {
        type: "integer"
    },

    "commented": {
        type: "relationships",
        target: "Issue",
        relationship: "commented",
        direction: "out",
        properties: {
            name: "string",
            date: {
                type: "datetime",
                default: () => new Date()
            }
        },
        eager: true
    },


};
