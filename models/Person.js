module.exports = {
    "id": {
        type: "number",
        primary: true
    },
    "firstName": "string",
    "lastName": "string",

    "email": "string",

    "username": {
        type: "string",
        //unique: 'true'
        // NO ESPACE
    }, // begins with @5ra

    "password": {
        type: "string",
    },

    "lastModified": "string",
    "profileImageUrl": {
        type: "string",
        uri: {
            scheme: ["http", "https"]
        }
    },
    "linkedin": "string",
    "website": "string",
    "organisation": "string",
    "bio": "string",
    "status": "string",

    "member": {
        type: "relationships",
        target: "Project",
        relationship: "member",
        direction: "out",
            properties: {
            role: "string",
            date: {
                type : "datetime",
                default: () => new Date()
            }
        },
        eager: true
    },

    "forked": {
        type: "relationships",
        target: "Project",
        relationship: "forked",
        direction: "out",
        properties: {
            forked_from: "string",
            date: {
                type : "datetime",
                default: () => new Date()
            }
        },
        eager: true
    },





};
