module.exports = {
    "id": {
        type: "number",
        primary: true
    },
    "name":
        {
            type: "string",
            unique: false, // because this project will exist with different users
            //required: true
            // NO ESPACE
        },

    /*
     */
    "description": {
        type: "string",
        default: "my awesome project"
    },
    "date": {
        type: "datetime",
        default: () => new Date()
    },
    "etat": "string",

    "stars": {
        type: "integer",
        default: 0
    },
    "last_update": {
        type: "datetime",
        default: () => new Date()
    },

    "visibility": {
        type: "string"
    },

    "creator_id": {
        type: "integer",
    },

    "link_identifier": {
        type: "string"
    },

    "member": {
        type: "relationships",
        target: "Person",
        relationship: "member",
        direction: "in",
        properties: {
            role: "string",
            date: {
                type: "datetime",
                default: () => new Date()
            }
        },
        eager: false
    },

    "branched": {
        type: "relationships",
        target: "Branche",
        relationship: "branched",
        direction: "in",
        properties: {
            name: "string",
            date: {
                type: "datetime",
                default: () => new Date()
            }
        },
        eager: true
    },

    "forked": {
        type: "boolean",
        default: false,
    },

    "forkes": {
        type: "integer",
        default: 0
    },


};
