module.exports = {
    "id": {
        type: "number",
        primary: true
    },
    "name": {
        type: "string",
        //unique: false, // because this project will exist with different users
        //required: true
    },

    "issue_number": {
        type: "number",
        default: 1
    },

    "state": {
        type: "string",
        default: "pending"
    },



    "description": {
        type: "string"
    },

    "hash_value": {
        type: "string",
    },

    "created_at": {
        type: "date",
        default: () => new Date()
    },

    "creator_id": {
        type: "integer"
    },
    "project_id": {
        type: "integer"
    },
    "branche_id": {
        type: "integer"
    },

    "issued": {
        type: "relationships",
        target: "Branche",
        relationship: "issued",
        direction: "out",
        properties: {
            name: "string",
            date: {
                type: "datetime",
                default: () => new Date()
            }
        },
        eager: true
    },

    "commented": {
        type: "relationships",
        target: "Comment",
        relationship: "commented",
        direction: "in",
        properties: {
            name: "string",
            date: {
                type: "datetime",
                default: () => new Date()
            }
        },
        eager: true
    }


};
