module.exports = {
    "id": {
        type: "number",
        primary: true
    },

    "name": {
        type: "string",
        default: "commit name"
    },

    "hash_value": {
        type: "string",
    },

    "description": {
        type: "string",
    },

    "created_at": {
        type: "date",
        default: () => new Date()
    },


    "creator_id": {
        type: "integer"
    },

    "timestamp": {
        type: "string"
    },

    "commited": {
        type: "relationships",
        target: "Branche",
        relationship: "commited",
        direction: "out",
        properties: {
            name: "string",
            date: {
                type: "datetime",
                default: () => new Date()
            }
        },
        eager: false
    },

    "issued": {
        type: "relationships",
        target: "Issue",
        relationship: "issued",
        direction: "in",
        properties: {
            name: "string",
            date: {
                type: "datetime",
                default: () => new Date()
            }
        },
        eager: false
    }


};
