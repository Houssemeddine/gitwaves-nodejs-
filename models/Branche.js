module.exports = {

    "id": {
        type: "number",
        primary: true
    },

    "name": {
        type : "string",
        default : "branche name"
    },

    "last_update": {
        type :"date",
        default : () => new Date()
    },

    "hash_value":{
        type : "string",
    },

    "created_at":{
        type :"date",
        default : () => new Date()
    },

    "creator_id" : {
        type: "integer"
    },
    "clone_id" : {
        type: "integer"
    },


    "branched": {
        type: "relationships",
        target: "Project",
        relationship: "branched",
        direction: "out",
        properties: {
            name: "string",
            date: {
                type : "datetime",
                default: () => new Date()
            }
        },
        eager: true
    },

    "commited":{
        type: "relationships",
        target: "Commit",
        relationship: "commited",
        direction: "in",
        properties: {
            name: "string",
            date: {
                type : "datetime",
                default: () => new Date()
            }
        },
        eager: true
    }

};
