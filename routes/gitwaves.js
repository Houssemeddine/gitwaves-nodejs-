/**
 * As this file requires an instance of neode, I've exported
 * a function that when called will return a new express Router.
 *
 * This can be added to express by calling
 *
 * app.use(require('./routes/api')(neode));
 *
 * @param {Neode} neode  Neode instance
 * @return {Router}      Express router
 */


module.exports = function (neode) {

    const router = require('express').Router();
    var express = require('express');
    var appsec = express.Router();
    const upload = require('../modules/upload');
    const uploadDir = require('../modules/uploadDir');
    var fileUtils = require('../modules/GFileSystUtils');
    const child_process = require('child_process');
    var archiver = require('archiver');
    var fs = require('fs');
    var sharedconsts = require('../sharedconsts');

    const cors = require('cors');

    var jwtmodule = require('../modules/jwt-module.js');


    var corsOptions = {
        origin: '*',
        optionsSuccessStatus: 200
    };
    router.use(cors(corsOptions));

    router.post('/api/new/issues/:branchid', function (req, res) {

        console.log("body"+req.body.name);
        console.log("param"+req.params.branchid);

        var from = "";
        var to = "";

        Promise.all([

            neode.findById("Branche", req.params.branchid),
            neode.create('Issue', req.body)
        ])
            .then(([branche, issue]) => {
                issue.relateTo(branche, 'issued', {role: "maintainer"})
                    .then(res => {
                        console.log("DONE RELATING USER TO PROJ ");
                    });
            }).then(result => {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.end(JSON.stringify("success"));
        }).catch(reason => {
            res.end(JSON.stringify(reason));
        });

    });

    router.get('/api/issues/:idproject', function (req, res) {

        neode.cypher('match (n:Issue) where n.project_id = '+req.params.idproject+' return n')
            .then(res => {

                // console.log(res);
                return Promise.all([
                    /**
                     * .hydrateFirst can be called to get the record with
                     * the alias provided in the first row, then return
                     * this object wrapped in a `Node` instance
                     */
                    neode.hydrate(res, 'n').toJson(),
                ])
                    .then(([Issues]) => {
                        console.log("Issue UPDATED: " + Issues);
                        return {Issues};
                    });

            }).then(
            res=>{
                return res.Issues;
            }
        )
            .then(json => {
                // Return a JSON response
                res.send(json);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });
    });

    router.get('/api/issue/:id', (req, res) => {

        neode.findById('Issue', req.params.id)
            .then(res => {
                if (!res)
                    return "{}";
                return res.toJson();
            })
            .then(json => {
                res.send(json);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });
    });
    router.post('/api/new/comment/:issueid', function (req, res) {

        var from = "";
        var to = "";

        Promise.all([

            neode.findById("Issue", req.params.issueid),
            neode.create('Comment', req.body)
        ])
            .then(([issue, comment]) => {
                comment.relateTo(issue, 'commented')
                    .then(res => {

                    });
            }).then(result => {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.end(JSON.stringify("success"));
        }).catch(reason => {
            res.end(JSON.stringify(reason));
        });

    });
    router.get('/api/comment/:idissues', function (req, res) {

        neode.cypher('match (n:Comment) where n.issues_id = '+req.params.idissues+' return n')
            .then(res => {

                // console.log(res);
                return Promise.all([
                    /**
                     * .hydrateFirst can be called to get the record with
                     * the alias provided in the first row, then return
                     * this object wrapped in a `Node` instance
                     */
                    neode.hydrate(res, 'n').toJson(),
                ])
                    .then(([Issues]) => {
                        console.log("Comment UPDATED: " + Issues);
                        return {Issues};
                    });

            }).then(
            res=>{
                return res.Issues;
            }
        )
            .then(json => {
                // Return a JSON response
                res.send(json);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });
    });
    router.post('/api/membres/new/:projectId', function (req, res) {
        console.log("id user : "+ req.body.user_id);
        console.log("id project : "+ req.params.projectId);
        console.log("ROle : "+ req.body.role);
        Promise.all([
            neode.findById('Person', req.body.user_id),
            neode.findById('Project', req.params.projectId),
        ])
            .then(([user, project]) => {
                user.relateTo(project, 'member', {role: "member"})
                    .then(res => {
                        console.log("DONE RELATING USER TO PROJ ");
                        //res.end(JSON.stringify("done"));
                        res.status(200).send(res)

                    });

            }).catch(reason => {
            resp.status(500).send(reason.stack);
        });
    });
    router.get('/api/membres', (req, res) => {

        const order_by = req.query.order || 'name';
        const sort = req.query.sort || 'ASC';
        const limit = req.query.limit || 10;
        const page = req.query.page || 1;
        const skip = (page - 1) * limit;

        const params = {};
        const order = {[order_by]: sort};
        console.log("aaaaaaaa")

        neode
            .all('Person', params, order, limit, skip)
            .then(res => {
                /*
                 *`all` returns a NodeCollection - this has a toJson method that
                 * will convert all Nodes within the collection into a JSON object
                 */
                return res.toJson();
            })
            .then(json => {
                res.send(json);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });
    });
    router.get('/api/membres/:link1/:link2', (req, res) => {

        neode.cypher('match (p:Project{link_identifier: "'+req.params.link1+'/'+req.params.link2+'"}) -[:member]-(n:Person) RETURN n')
            .then(res => {

                // console.log(res);
                return Promise.all([
                    /**
                     * .hydrateFirst can be called to get the record with
                     * the alias provided in the first row, then return
                     * this object wrapped in a `Node` instance
                     */
                    neode.hydrate(res, 'n').toJson(),
                ])
                    .then(([Issues]) => {
                        console.log("membres UPDATED: " + Issues);
                        return {Issues};
                    });

            }).then(
            res=>{
                return res.Issues;
            }
        )
            .then(json => {
                // Return a JSON response
                res.send(json);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });
    });
    router.delete('/api/membres/delete/:idproject/:iduser', function (req, res) {
        console.log(req.params.id)

        neode.cypher("MATCH (p:Project)-[r:member]-(m:Person) where ID(p)="+req.params.idproject+   " AND ID(m)="+req.params.iduser +
            "    DELETE r");

    });
    router.get('/api/branch/:id', (req, res) => {

        neode.findById('Branche', req.params.id)
            .then(res => {
                if (!res)
                    return "{}";
                return res.toJson();
            })
            .then(json => {
                res.send(json);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });
    });
    router.get('/api/commits/:id', (req, res) => {

        neode.cypher('match (n:Commit)-[r:commited]-(b:Branche) where ID(b) = '+req.params.id +' return n ')
            .then(res => {

                // console.log(res);
                return Promise.all([
                    /**
                     * .hydrateFirst can be called to get the record with
                     * the alias provided in the first row, then return
                     * this object wrapped in a `Node` instance
                     */
                    neode.hydrate(res, 'n').toJson(),
                ])
                    .then(([Issues]) => {
                        console.log("membres UPDATED: " + Issues);
                        return {Issues};
                    });

            }).then(
            res=>{
                return res.Issues;
            }
        )
            .then(json => {
                // Return a JSON response
                res.send(json);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });
    });
    return router;
}
