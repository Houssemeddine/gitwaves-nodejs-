/**
 * As this file requires an instance of neode, I've exported
 * a function that when called will return a new express Router.
 *
 * This can be added to express by calling
 *
 * app.use(require('./routes/api')(neode));
 *
 * @param {Neode} neode  Neode instance
 * @return {Router}      Express router
 */



module.exports = function (neode) {

    const router = require('express').Router();
    var express = require('express');
    var appsec = express.Router();
    const upload = require('../modules/upload');
    const uploadDir = require('../modules/uploadDir');

    const cors = require('cors');

    var jwtmodule = require('../modules/jwt-module.js');


    var corsOptions = {
        origin: '*',
        optionsSuccessStatus: 200
    };

    router.use(cors(corsOptions));

    // need to delete everything

    /*
    neode.schema.drop();
     */

    neode.schema.install()
        .then(() => console.log('Schema installed!'))
        .catch(reason => {
            console.log(reason);
        });

    appsec.route('/api/user').post(function (req, res) {
        console.log('POST /');
        console.dir(req.body);
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end('thanks');
    });


    appsec.route('').post(function (req) {

    });

    // todo : check if user with some username already exist
    router.post('/api/user/register', function (req, res) {
        // NO , REJECTED ! when a user is created : init the sub nodes PROJECTS ( Sub projects node can be of type cloned  or created , ....
        console.log("Create new User");

        // console.log(req.body);

        neode.create("Person", req.body);
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(JSON.stringify(req.body));
    });


    router.get('/api/users', (req, res) => {

        const order_by = req.query.order || 'firstname';
        const sort = req.query.sort || 'ASC';
        const limit = req.query.limit || 10;
        const page = req.query.page || 1;
        const skip = (page - 1) * limit;

        const params = {};
        const order = {[order_by]: sort};


        neode
            .all('Person', params, order, limit, skip)
            .then(res => {
                /*
                 *`all` returns a NodeCollection - this has a toJson method that
                 * will convert all Nodes within the collection into a JSON object
                 */
                return res.toJson();
            })
            .then(json => {
                res.send(json);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });
    });

    //deprecated : NOT USED
    router.post('/api/authenticate/', (req, res) => {

        console.dir(req.body.username);
        console.dir(req.body.password);

        neode.cypher('MATCH (person:Person { login:"' + req.body.username + '", password:"' + req.body.password + '" }) ' +
            'return person', req.body)
            .then(res => {

               // console.log(res);
                return Promise.all([
                    /**
                     * .hydrateFirst can be called to get the record with
                     * the alias provided in the first row, then return
                     * this object wrapped in a `Node` instance
                     */
                    neode.hydrateFirst(res, 'person').toJson(),
                ])
                    .then(([person]) => {

                        var sOptions = {
                            issuer: "Authorizaxtion/Resource/This server",
                            subject: "changeme@me.com",
                            audience: "Client_Identity" // this should be provided by client
                        };

                        //is the data that‘s stored inside the JWT : can be login,pwd ..etc
                        var payload = {
                            data1: "Data 1",
                            data2: "Data 2",
                            data3: "Data 3",
                            data4: "Data 4",
                        };


                        var token = jwtmodule.sign(payload, sOptions);
                        // Format into a friendly object
                       // console.log("TOKEN  : " + token);
                        return {person, token};
                    });
            })
            .then(json => {
                // Return a JSON response

                console.log("aaaaaaaaaaaaaaaaaaa");
                //console.log(json);
                res.send(json);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });


    });

    router.get('/api/authenticate/:username/:pwd', (req, res) => {
        neode.cypher('MATCH (user:Person { username:{username} , password:{pwd} }) return user', req.params)
            .then(res => {

                console.log(res);
                return Promise.all([
                    /**
                     * .hydrateFirst can be called to get the record with
                     * the alias provided in the first row, then return
                     * this object wrapped in a `Node` instance
                     */
                    neode.hydrateFirst(res, 'user').toJson(),
                ])
                    .then(([user]) => {

                        var sOptions = {
                            issuer: "Authorizaxtion/Resource/This server",
                            subject: "changeme@me.com",
                            audience: "Client_Identity" // this should be provided by client
                        };

                        //is the data that‘s stored inside the JWT : can be username,pwd ..etc
                        var payload = {
                            data1: "Data 1",
                            data2: "Data 2",
                            data3: "Data 3",
                            data4: "Data 4",
                        };


                        var token = jwtmodule.sign(payload, sOptions);
                        // Format into a friendly object
                        //console.log("TOKEN  : " + token);

                        return {user, token};
                    });
            })
            .then(json => {
                // Return a JSON response
                console.log(json);
                res.send(json);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });


    });

    router.get('/desktop/api/authenticate/:username/:pwd', (req, res) => {
        neode.cypher('MATCH (user:Person { username:{username} , password:{pwd} }) return user', req.params)
            .then(res => {

                console.log(res);
                return Promise.all([
                    /**
                     * .hydrateFirst can be called to get the record with
                     * the alias provided in the first row, then return
                     * this object wrapped in a `Node` instance
                     */
                    neode.hydrateFirst(res, 'user').toJson(),
                ])
                    .then(([user]) => {
                        return {user};
                    });
            })
            .then(json => {
                // Return a JSON response
                res.send(json.user);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });


    });

    router.get('/api/user/findby/:findby/:param', (req, res) => {

        var prepareStatement = "";

        if (req.params.findby === "id") {
            console.log("here");
            prepareStatement = "MATCH (person:Person) where ID(person) = " + req.params.param + " return person";
        } else {
            prepareStatement = 'MATCH (person:Person { ' + req.params.findby + ':{param} }) return person';
        }

        //neode.cypher('MATCH (a:Movie {id:{a_id}})');

        console.log(req.params);
        neode.cypher(prepareStatement, req.params)
            .then(res => {

                console.log(res);
                return Promise.all([
                    /**
                     * .hydrateFirst can be called to get the record with
                     * the alias provided in the first row, then return
                     * this object wrapped in a `Node` instance
                     */
                    neode.hydrateFirst(res, 'person').toJson(),
                ])
                    .then(([person]) => {
                        // Format into a friendly object
                        return {person};
                    });
            })
            .then(json => {
                // Return a JSON response
                res.send(json);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });


    });


    router.get('/api/user/:id', (req, res) => {

        neode.findById('Person', req.params.id)
            .then(res => {
                if (!res)
                    return "{}";
                return res.toJson();
            })
            .then(json => {
                res.send(json);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });
    });

    router.put('/api/user/:id', (req, res) => {


        console.log("EXECUT DIS : " + "match (n:Person ) where ID(n) = " + req.params.id + " " +
            "set " +
            "n.bio = '" + req.body.bio + "', " +
            "n.email = '" + req.body.email + "', " +
            "n.firstName = '" + req.body.firstName + "', " +
            "n.lastName = '" + req.body.lastName + "', " +
            "n.linkedin = '" + req.body.linkedin + "', " +
            "n.organisation = '" + req.body.organisation + "', " +
            "n.profileImageUrl = '" + req.body.profileImageUrl + "', " +
            "n.status = '" + req.body.status + "', " +
            "n.twitter = '" + req.body.twitter + "', " +
            "n.website = '" + req.body.website + "' " +

            "return n");

        neode.cypher("match (n:Person ) where ID(n) = " + req.params.id + " " +
            "set " +
            "n.bio = '" + req.body.bio + "', " +
            "n.email = '" + req.body.email + "', " +
            "n.firstName = '" + req.body.firstName + "', " +
            "n.lastName = '" + req.body.lastName + "', " +
            "n.linkedin = '" + req.body.linkedin + "', " +
            "n.organisation = '" + req.body.organisation + "', " +
            "n.profileImageUrl = '" + req.body.profileImageUrl + "', " +
            "n.status = '" + req.body.status + "', " +
            "n.twitter = '" + req.body.twitter + "', " +
            "n.website = '" + req.body.website + "' " +

            "return n")
            .then(res => {

                // console.log(res);
                return Promise.all([
                    /**
                     * .hydrateFirst can be called to get the record with
                     * the alias provided in the first row, then return
                     * this object wrapped in a `Node` instance
                     */
                    neode.hydrateFirst(res, 'n').toJson(),
                ])
                    .then(([person]) => {
                        console.log("PERSON UPDATED: " + person);
                        return {person};
                    });
            })
            .then(json => {
                // Return a JSON response
                res.send(json);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });


    });


    /*
    UPLOAD SHIT here
     */


    router.post('/upload/:filename', upload);
    router.post('/upload', upload);
    router.post('/uploadDir', uploadDir);

    var mkdirp = require('mkdirp');
    var fs = require('fs');
    var path = require('path');
    var sharedconsts = require('../sharedconsts');


    /*
    listFile: 'api/file/list',
    uploadFile: 'api/file/upload',
    downloadFile: 'api/file/download',
    deleteFile: 'api/file/remove',
    createFolder: 'api/file/directory',
    renameFile: 'api/file/rename',
    searchFiles: 'api/file/search'
     */

    router.get('/api/file/list?parentPath=:path', (req, res) => {

    });

    router.get('/api/file/list/:id', (req, res) => {
        // return all
    });

    router.delete('/api/file/remove/:id', (req, res) => {

    });

    router.get('/api/file/directory/:id', (req, res) => {

    });

    router.delete('/api/file/search/:id', (req, res) => {

    });

    router.post('/initrepo/:id', (req, res) => {
        mkdirp(sharedconsts.__dirname + "\\" + req.params.id, function (err) {
            if (err) console.error(err);
            else {
                console.log('Repo INITED');
            }
        });
    });


    router.get('/test/date', () => {
        console.log(new Date());
        console.log(new Date().getTime());
        console.log(new Date().toDateString());
        console.log(new Date().getMilliseconds());
        console.log(new Date().getUTCDate());
    });


    router.get('/tree', () => {
        const dree = require('dree');
        console.log("calle");
        const tree = dree.scan('./');
        console.log("TREE " + JSON.stringify(tree));

    });

    router.get('/test/relation', (req, res) => {

        /*

        neode.create('Person', {
            firstName: 'ELYES',
            lastName: 'Derouiche'
        })
            .then(returned => {
                console.log(returned.get('firstName')); // 'Adam'
            });
         */

        Promise.all([
            neode.create('Person', {firstName: 'Adame'}),
            neode.create('Project', {name: 'Projects'})
        ])
            .then(([user, project]) => {
                user.relateTo(project, 'owner', {name: "2010"})
                    .then(res => {
                        /*
                        console.log(
                            res.from().get('firstName'),
                            ' has known ',
                            res.to().get('name'),
                            'since',
                            res.get('name'));  // Adam has known Joe since 2010
                         */
                        console.log("DONE CHECK NEO 4J ");
                    });
            });

    });


    return router;
};
