/**
 * As this file requires an instance of neode, I've exported
 * a function that when called will return a new express Router.
 *
 * This can be added to express by calling
 *
 * app.use(require('./routes/api')(neode));
 *
 * @param {Neode} neode  Neode instance
 * @return {Router}      Express router
 */


const __gitDirName = require("../sharedconsts").__gitDirName;
module.exports = function (neode) {

    const router = require('express').Router();
    var express = require('express');
    var appsec = express.Router();
    const upload = require('../modules/upload');
    const uploadDir = require('../modules/uploadDir');
    var fileUtils = require('../modules/GFileSystUtils');
    const child_process = require('child_process');
    var archiver = require('archiver');
    var fs = require('fs');
    var sharedconsts = require('../sharedconsts');

    const cors = require('cors');

    var jwtmodule = require('../modules/jwt-module.js');


    var corsOptions = {
        origin: '*',
        optionsSuccessStatus: 200
    };

    router.use(cors(corsOptions));

    router.post('/api/projects/new', function (req, resp) {

        // TODO : add visibility with the request data
        // NEED USER ID TO BE SENT SO WE LINK THE PROJECT TO USER's PROJECTS

        // TODO : DONE : send user ID and get it from here , USE req.body instead in parameters ( send them corresponding to project model)

        //TODO : DONE : make sure project name doesn't exist


        // console.log("RECEIVED THIS ID : " + req.body.id);
        const userId = req.body.id;
        delete req.body.id;
        const linkIdBranchePath = req.body.username + "/" + req.body.name + "/master";
        req.body.link_identifier = req.body.username + "/" + req.body.name;

        // todo generate hash here

        // todo : DONE / NEED TO RECEIVE userID in req.body


        Promise.all([
            neode.findById('Person', userId),
            neode.create('Project', req.body),
            neode.create('Branche', {name: "master", hash_value: "12345678CD", creator_id: userId})
        ])
            .then(([user, project, branch]) => {
                user.relateTo(project, 'member', {role: "maintainer"})
                    .then(res => {
                        console.log("DONE RELATING USER TO PROJ ");
                    });
                branch.relateTo(project, 'branched', {})
                    .then(res => {
                        // todo return the actual created project
                        //console.log("Returning new created project : " + JSON.stringify(res));
                        console.log("Creating User DIR " + linkIdBranchePath);
                        fileUtils.createDir(linkIdBranchePath);
                    }).then(res => {
                    console.log("ITs all done");
                    resp.end(JSON.stringify("done"));
                })
            }).catch(reason => {
            resp.status(500).send(reason.stack);
        });
    });


    router.get("/api/list/branches/:linkid1/:linkid2", function (req, res) {
        neode.cypher("match (p:Project)\n" +
            "where p.link_identifier = '" + req.params.linkid1 + "/" + req.params.linkid2 + "'\n" +
            "with p\n" +
            "match (p)-[:branched]-(b:Branche)\n" +
            "return b").then(result => {

            return Promise.all([
                neode.hydrate(result, 'b').toJson(),

            ]).then((project) => {
                // Format into a friendly object
                return project;
            });
        }).then(json => {
            // Return a JSON response
            // console.log("RETURING : " + json);
            //let newObj = renameObjectKey()
            res.status(200).send(json[0]);
        });
    });


    router.post('/api/new/branch/:projectid/from/:branchid', function (req, res) {

        console.log(req.params.projectid + " FROM BRANCHE : " + req.params.branchid);

        var from = "";
        var to = "";

        Promise.all([
            neode.findById('Project', req.params.projectid),
            neode.findById("Branche", req.params.branchid),
            neode.create('Branche', req.body)
        ])
            .then(([project, sourceBranche, branche]) => {
                project.relateTo(branche, 'branched', {});

                const projectLink = project.get("link_identifier");

                const brancheName = sourceBranche.get("name");
                const newBranchName = req.body.name;

                from = projectLink + "/" + brancheName + "/"; // => root/hello/master/
                to = projectLink + "/" + newBranchName + "/"; // ==> root/hello/newbranch/

                return newBranchName
            })
            .then(source => {

                console.log("DONE CREATING BRANCHE ! NEXT COPY SOURCE FROM CLONED BRANCHE");
                console.log("Name of branch to clone " + source);

                // should only copy last commit


                // fileUtils.recursCopyFiles(from, to);

                /**********************     UPDATE  : ONly recurse copy recent commit *********** */

                console.log("Dirs from path : " + __gitDirName + "/" + from);

                const dirs = fileUtils.getDirectoriesFromPath(__gitDirName + "/" + from);

                let recentDirName = new Date("1970-01-01"); //1970 unix
                let nameHolder = "";
                for (const d in dirs) {

                    console.log(dirs[d]);
                    const selectedDate = new Date(parseInt(dirs[d]) * 1000);

                    console.log("Comparin those 2 : " + selectedDate + " / " + recentDirName);
                    if (selectedDate > recentDirName) {
                        recentDirName = selectedDate;
                        console.log("Recent > Selected");
                        nameHolder = dirs[d];
                    }
                }

                console.log("Most recent one is : " + nameHolder);

                //root/hello/master/
                // ==> root/hello/newbranch/

                fileUtils.recursCopyFilesWithDirName(from,
                    to, nameHolder);


            }).then(result => {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.end(JSON.stringify("success"));
        }).catch(reason => {
            res.end(JSON.stringify(reason));
        });

    });


    /*
    fork project
    /api/fork/${projectId}/user/${userId}
     */
    router.post('/api/fork/:projectid/user/:userid', function (req, res) {

        console.log("FORKIN " + req.params.projectid + "/" + req.params.userid);
        console.log("received this body : " + req.body.link_identifier);

        neode.cypher(
            "MATCH (a:Person)-[o:member]-(p:Project)-[]-()\n" +
            "where ID(p)= " + req.params.projectid
            + "\n" +
            "WITH a, id(a) as aId, p\n" +
            "CALL apoc.path.subgraphNodes(p,{\n" +
            "relationshipFilter:'branched<|member<'\n" +
            "}) YIELD node\n" +
            "WITH aId, collect(node) as nodes , p \n" +
            " set p.forkes = p.forkes+1 " +
            "// executes right\n" +
            "\n" +
            "// we can perform UPDATE TO NODES HERE\n" +
            "\n" +
            "CALL apoc.refactor.cloneNodesWithRelationships(nodes) YIELD input, output\n" +
            "\n" +
            "WITH aId, collect({input:input, output:output}) as createdData, collect(output) as createdNodes\n" +
            "\n" +
            "WITH createdNodes, [item in createdData WHERE item.input = aId | item.output][0] as aClone // clone of root A node\n" +
            "\n" +
            "UNWIND createdNodes as created\n" +
            "\n" +
            "OPTIONAL MATCH (created)-[r]-(other)\n" +
            "WHERE NOT other in createdNodes\n" +
            "DELETE r\n" +
            "\n" +
            "// get rid of relationships that arent between cloned nodes\n" +
            "// now to refactor relationships from aClone to the new B root node\n" +
            "\n" +
            "WITH DISTINCT aClone, createdNodes, created\n" +
            "\n" +
            "WITH collect(created) AS nodes\n" +
            "UNWIND nodes AS n\n" +
            "WITH n\n" +
            "WHERE 'Project' IN LABELS(n)\n" +
            "\n" +
            "with n\n" +
            "match (p:Person)\n" +
            "Where ID(p) = " + req.params.userid
            + "\n" +
            "with p, ID(p) as idP , n\n" +
            "create (p)-[ff:forked { forked_from:n.link_identifier , role:'maintainer' }]->(n)\n" +
            "with p, ID(p) as idP , n\n" +
            "set n.link_identifier = p.username+\"/\"+n.name\n , n.forked = true \n " +
            "return n"
        ).then(result => {
            // check result and extract new project forked ID

            return Promise.all([
                neode.hydrateFirst(result, 'n').toJson(),
            ]).then(([project]) => {
                // Format into a friendly object
                return project;
            });
        }).then(json => {

            console.log("preparing the clone file system");

            fileUtils.recursCopyFiles(req.body.link_identifier, json.link_identifier);

            // Return a JSON response
            // console.log("RETURING : " + json);
            res.status(200).send(json);
        });

    });


    /*
    POST : COmmit

    INPUT JSON DATA :

    {
        "creator_id": userid,
        "commit_date": exact date,
        "name": commit title,
        "description": desc,
        "hash_value" : generated from date & checksum file,
        and the file to upload
    }
     */
    router.post('/api/project/commit/:username/:project/:branchename', function (req, res) {


        var link_identifier = req.params.username + "/" + req.params.project;

        console.log(req.params.branchename);


        let result = neode.cypher("MATCH (p:Project)<-[:branched]-(b:Branche)\n" +
            " WHERE p.link_identifier = '" + link_identifier + "'" +
            " AND b.name = '" + req.params.branchename + "'" +
            "return b "
        ).then((branche) => {

            console.log(branche);

            const timestamp = Date.now();

            req.body.timestamp = timestamp.toString();

            Promise.all([
                neode.create('Commit', req.body),
                neode.hydrateFirst(branche, 'b')
            ]).then(([commite, branche]) => {

                commite.relateTo(branche, 'commited', {})
                    .then(result => {

                        //Create the file dir : /user/project/branch/commit/
                        console.log(link_identifier + "/" + req.params.branchename + "/" + timestamp);
                        var commitdir = link_identifier + "/" + req.params.branchename + "/" + timestamp;

                        // todo : move the file here
                        fileUtils.createDir(commitdir);

                        res.end("Commit & its directory")
                    });
            });

            /*
            Promise.all([
                neode.hydrateFirst(branche, 'b').toJson(),
            ]).then((b) => {
                //console.log(JSON.stringify(b));

                return JSON.stringify(b);
            }).then(json => {
                console.log(json);
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.end(json);
            }).catch(reason => {
                console.log(reason)
            })
             */

        });
    });


    /*
    GET PROJECT INFO
    api/goto/{username}/{projectName}
     */
    router.get('/api/project/:username/:project', function (req, res) {

        // console.log("Checking " + req.params.username + "/" + req.params.project);
        var link_identifier = req.params.username + "/" + req.params.project;


        /*
        IN CASE OF PROBLEM  TRY DIS
        MATCH (p:Project)<-[:branched]-(hereb:Branche)
WHERE p.link_identifier = "admin/hallo"
with p, b
optional match(p)<-[r:forked]-()
return p, b , count(r)
         */

        neode.first('Project', 'link_identifier', link_identifier)
            .then(project => {
                project.toJson()
                    .then(json => {
                        console.log(json);

                        var creator_id = json.creator_id;

                        console.log(creator_id);

                        neode.findById("Person", creator_id)
                            .then(creator => {

                                // console.log(creator.toJson());

                                creator.toJson()
                                    .then(creator => {

                                        console.log("here");
                                        console.log(creator);

                                        delete creator.member;
                                        delete creator.forked;
                                        delete creator.password;

                                        json.creator_id = creator;

                                        for (let pr in json.branched) {

                                            console.log(json.branched[pr].node);

                                            var branche = json.branched[pr].node;

                                            json.branched[pr] = json.branched[pr].node;

                                            delete json.branched[pr].branched;
                                            //json.branched[pr].commited;

                                            // for every commmit inside this branch do

                                            for (let prs in json.branched[pr].commited) {

                                                console.log(json.branched[pr].commited[prs].node);

                                                var commit = json.branched[pr].commited[prs].node;

                                                json.branched[pr].commited[prs] = json.branched[pr].commited[prs].node;

                                                // delete json.branched[pr].branched;
                                                // json.branched[pr].commited;
                                            }
                                        }

                                        res.writeHead(200, {'Content-Type': 'application/json'});

                                        res.end(JSON.stringify(json));

                                    });
                            });

                    })
            });


        /*let result = neode.cypher("MATCH (p:Project)<-[:branched]-(b:Branche)\n" +
              " WHERE p.link_identifier = '" + link_identifier + "'" +
              " with p, b \n" +
              "optional match(p)<-[r:forked]-()\n" +
              "return p , b "
          ).then(result => {
              console.log("aaaaaaaaaa");
              Promise.all([
                  neode.hydrateFirst(result, 'p').toJson(),
                  neode.hydrate(result, 'b').toJson(),
              ]).then(([p, b]) => {
                  //   console.log(p);

                  console.log("aaaaaaaaaa");
                  var returJson = [];

                  var jsonObject = {};

                  jsonObject["project"] = {};
                  jsonObject["branches"] = {};

                  jsonObject["project"] = (p);
                  jsonObject["branches"] = (b);

                  // console.log(jsonObject);
                  console.log(JSON.stringify(jsonObject));
                  res.send(JSON.stringify(jsonObject));
              })
          }).catch(reason => {
              res.send(reason.stack);
          })*/

    });


    /*
    desktop/api/perform/commit
     */

    router.post('/desktop/api/perform/commit', function (req, res) {
        console.log("Commiting from desktop");
        console.log(req.body);

        // here we need to got throu all thr branches and submit them one by one

        var projectID = req.body._id;
        var projectIdentifier = req.body.link_identifier;

        for (br in req.body.branched) {
            console.log("Here we runngin through all the branches");
            console.log(req.body.branched[br]._id);

            var brancheId = req.body.branched[br]._id;

            //neode.create('Branche',req.body.branched[br]);
            for (pc in req.body.branched[br].commited) {
                console.log("Commit least for eaching");
                console.log(req.body.branched[br].commited[pc]);

                var commit = req.body.branched[br].commited[pc];

                //*********** HERE GOES ALL THE WORK

                console.log(new Date());
                console.log(commit.created_at);
                console.log("Converting to :");
                console.log(new Date(("14-04-2019 10:00:36")));
                commit.created_at = new Date();

                Promise.all([
                    neode.create('Commit', commit),
                    neode.findById('Branche', brancheId)
                ])
                    .then(([commit, branche]) => {
                        commit.relateTo(branche, 'commited', {})
                            .then(res => {
                                console.log("DONE Relating commit --> branche ");
                            });
                    });
            }
        }

        //var project = JSON.parse(req.body);

        // console.log(project.name);


        // THIS IS FUKCUNK OBLIG SO WE CAN UPLOAD AND CLEAN
        // send the new project would would be better OR ...
        res.end("done");
    });


    /*
    Get all project for given user id
     */
    router.get('/api/list/projects/:id', function (req, res) {
        // console.log(req.params.id);

        /*
        person.get('owner');
                console.log(person.get('owner').);
         */
        console.log("Returning User Projects");
        neode.findById('Person', req.params.id)
            .then(person => {
                return person.toJson();
            }).then(json => {

            var container = {};

            container['member'] = {};
            container['forked'] = {};

            container['member'] = (json['member']);
            container['forked'] = (json['forked']);

            //res.send(JSON.stringify(container));
            return container;

        }).then(container => {

            // looop for

            /*

                        for (let n in container.member) {
                            console.log("Need to find the user with : " + container.member[n].node.creator_id);
                            let userid = container.member[n].node.creator_id;
                            neode.findById('Person', userid)
                                .then(person => {

                                    console.log(person.get('username'));

                                    let creator =
                                        {
                                            firtName: person.get('firstName'),
                                            lastName: person.get('lastName'),
                                            username: person.get('username')
                                        };

                                    delete container.member[n].node.creator_id;
                                    container.member[n].node.creator = creator;

                                });
                        }

                        for (let n in container.forked) {
                            console.log("Need to find the user with : " + container.forked[n].node.creator_id);
                            let userid = container.forked[n].node.creator_id;
                            neode.findById('Person', userid)
                                .then(person => {

                                    //console.log(person.get('username'));

                                    let creator =
                                        {
                                            firtName: person.get('firstName'),
                                            lastName: person.get('lastName'),
                                            username: person.get('username')
                                        };

                                    delete container.forked[n].node.creator_id;
                                    container.forked[n].node.creator = creator;
                                });
                        }

             */
            // DONE PARCOURRING ALL OF THE LISTS

            console.log("returing this");
            res.send(JSON.stringify(container));


        }).catch(reason => {
            res.status(500).send(reason.stack);
        })
    });


    /*
    SAME AS PRECEDANT but by username
    Get all project for given user username
     */
    router.get('/api/list/user/projects/:username', function (req, res) {
        // console.log(req.params.id);

        console.log("Returning User Projects");

        neode.first('Person', 'username', req.params.username)
            .then(person => {
                person.toJson()
                    .then(json => {
                        for (let pr in json.member) {
                            var holder = json.member[pr].node;
                            delete json.member[pr];
                            json.member[pr] = holder;
                        }

                        for (let pr in json.forked) {
                            var holder = json.forked[pr].node;
                            delete json.forked[pr];
                            json.forked[pr] = holder;
                        }
                        res.writeHead(200, {'Content-Type': 'application/json'});
                        res.end(JSON.stringify(json));
                    })
            });

        /*
         neode.findById('Person', req.params.id)
             .then(person => {
                 return person.toJson();
             }).then(json => {

             var container = {};

             container['member'] = {};
             container['forked'] = {};

             container['member'] = (json['member']);
             container['forked'] = (json['forked']);

             //res.send(JSON.stringify(container));
             return container;

         }).then(container => {

         // DONE PARCOURRING ALL OF THE LISTS

         console.log("returing this");
         res.send(JSON.stringify(container));


     })

         .catch(reason => {
             res.status(500).send(reason.stack);
         })
         */
    });


    router.get('/api/get/content/:link1/:link2?/:branchname?/:commit?', function (req, res) {

        const link1 = req.params.link1;
        const link2 = req.params.link2;
        const brancheName = req.params.branchname;
        const commitNumber = req.params.commit;


        /*
        GRAB THE LAST COMMIT
         */


        var path = "";
        path += link1;

        if (link2 !== undefined) {
            path += "/" + link2;
        }

        if (brancheName !== undefined) {
            path += "/" + brancheName;
        }


        let recentDirName = new Date("1970-01-01"); //1970 unix
        let nameHolder = "";

        if (commitNumber !== undefined) {
            path += "/" + commitNumber;
            // nameHolder = commitNumber;
        }


        console.log("Dirs from path : " + __gitDirName + "/" + path);

        const dirs = fileUtils.getDirectoriesFromPath(__gitDirName + "/" + path);

        for (const d in dirs) {

            //console.log(dirs[d]);
            const selectedDate = new Date(parseInt(dirs[d]) * 1000);

            console.log("Comparin those 2 : " + selectedDate + " / " + recentDirName);
            if (selectedDate > recentDirName) {
                recentDirName = selectedDate;
                //console.log("Recent > Selected");
                nameHolder = dirs[d];
            }
        }

        console.log("Most recent one is : " + nameHolder);
        console.log("made it here " + path + "/" + nameHolder);
        var tree = fileUtils.fileSystemTreeFromPath(path + "/" + nameHolder);


        var link_identifier = link1 + "/" + link2;
        console.log("LINK : " + link_identifier);
        console.log("Branche NAme : " + brancheName);

        if (commitNumber !== undefined) {
            nameHolder = commitNumber;
        }

        // this does return null when no commit found

        neode.cypher("MATCH (a:Project)<-[o:branched]-(p:Branche)<-[:commited]-(c:Commit)\n" +
            "where a.link_identifier  = '" + link_identifier + "'" +
            "AND p.name = '" + brancheName + "'" +
            "AND c.timestamp = '" + nameHolder + "'" +
            "return c\n"
            //+"ORDER by c.created_at ASC"
        )
            .then(commits => {
                Promise.all([
                    neode.hydrateFirst(commits, 'c').toJson()
                ]).then(commitNode => {
                    //return only one node
                    //console.log(commitNode);


                    var creator_id = commitNode[0].creator_id;

                    neode.findById("Person", creator_id)
                        .then(commitCreator => {

                            console.log(commitCreator.toJson());

                            commitCreator.toJson()
                                .then(json => {
                                    console.log(json);

                                    delete json.member;
                                    delete json.password;
                                    delete json.forked;

                                    commitNode[0].creator_id = json;

                                    tree["last_commit"] = commitNode[0];

                                    tree = JSON.stringify(tree);

                                    var newFormattedTree = tree.replace("\"name\":", "\"text\":");

                                    //newFormattedTree = JSON.stringify(newFormattedTree);
                                    delete newFormattedTree.path;
                                    delete newFormattedTree.size;
                                    delete newFormattedTree.type;

                                    //newFormattedTree.last_commit = commitNode;


                                    //****************************************
                                    /*
                                     dataSource: [
                                            {text: 'Parent node', children: [{text: 'Node 1.1'}, {text: 'Node 1.2'}, {text: 'Node 1.3'}]},
                                            {text: 'Node 2', children: [{text: 'Node 2.1'}, {text: 'Node 2.2'}]},
                                            {text: 'Node 3', children: [{text: 'Node 3.1'}, {text: 'Node 3.2'}]}
                                          ],
                                     */
                                    var array = JSON.parse(newFormattedTree);

                                    console.log("-------------------------------");
                                    console.log("-------------------------------");
                                    //console.log(array);


                                    var sources = [];

                                    for (item in array.children) {
                                        console.log(array.children[item]);
                                        let content = array.children[item];
                                        console.log(content);
                                        var dataSource = {};
                                        if (content.type === "file") {
                                            dataSource.text = content.name;
                                            dataSource.link = content.path.substr(15);
                                        } else {

                                        }
                                        sources.push(dataSource);
                                    }

                                    //  console.log(sources);
                                    // console.log("First result : "+JSON.stringify(sources));


                                    var result = {};
                                    result.tree = sources;
                                    result.last_commit = commitNode[0];
                                    //****************************************


                                    console.log("Returing : " + JSON.stringify(result));
                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    // console.log("MY TREE : "+newFormattedTree);
                                    res.end(JSON.stringify(result));

                                });
                        });
                });
            }).catch(e => {
            res.status(200).send("{}");
        });

        /*

                neode.all('Commit', params, order, limit, 0)
                    .then((commite) => {
                        return commite.toJson();
                    })
                    .then(json => {
                        //res.send(json);
                        console.log(json);
                    })
                    .catch(e => {
                        //  res.status(500).send(e.stack);
                    });
         */
    });

    router.post('/api/list/all/projects', function (req, res) {
        /*
        JSON BODY
        "order" : "ASC", "DESC",
        "limit" : 20
        "skip" : 0 .. 10 .. 20 ..
        */
        neode.all('Project', {}, {}, 20, 0)
            .then(collection => {
                collection.toJson().then(json => {
                    console.log(json);
                    return JSON.stringify(json)
                }).then(json => {
                    res.end(json);
                });
            })
    });


    function extractLatest (path){

        let recentDirName = new Date("1970-01-01"); //1970 unix
        let nameHolder = "";

        console.log("Dirs from path : " + __gitDirName + "/" + path);

        const dirs = fileUtils.getDirectoriesFromPath(__gitDirName + "/" + path);

        for (const d in dirs) {

            //console.log(dirs[d]);
            const selectedDate = new Date(parseInt(dirs[d]) * 1000);

            console.log("Comparin those 2 : " + selectedDate + " / " + recentDirName);
            if (selectedDate > recentDirName) {
                recentDirName = selectedDate;
                //console.log("Recent > Selected");
                nameHolder = dirs[d];
            }
        }

        console.log("Return latest commit path ; " + __gitDirName + path + "/" + nameHolder);
        return __gitDirName +"/"+ path + "/" + nameHolder;
    }

    router.get("/branche/clone/:username/:projectname/:branche", (req, res) => {
        var link_identifier = req.params.username + "/" + req.params.projectname;
        var projectName = req.params.projectname;
        const gitDir = sharedconsts.__gitDirName + '/';
        const gitTmps = sharedconsts.__gitTmps + '/';

        const brancheName = req.params.branche;

        const path = extractLatest(link_identifier+"/"+brancheName);

        console.log("got latet commit "+path);

        const folderPath = path;
        var output = fs.createWriteStream(gitTmps + projectName + "-" + brancheName + ".zip");

        console.log("OUTPUT : " + gitTmps + projectName + ".zip");

        var archive = archiver('zip', {
            zlib: {level: 9} // Sets the compression level.
        });

        output.on('close', function () {
            console.log(archive.pointer() + ' total bytes');
            console.log('archiver has been finalized and the output file descriptor has closed.');

            console.log("Downlodinth this : " + gitTmps + projectName + '.zip');
            res.download(gitTmps + projectName + "-" + brancheName + ".zip");
        });

        output.on('end', function () {
            console.log('Data has been drained');
        });

        archive.on('warning', function (err) {
            if (err.code === 'ENOENT') {
                // log warning
            } else {
                // throw error
                throw err;
            }
        });

        // good practice to catch this error explicitly
        archive.on('error', function (err) {
            throw err;
        });

        console.log("ZIPPIN THIS : " + folderPath);


        archive.directory(folderPath, false);


        var pdf = sharedconsts.__gitDirName + '/licence.pdf';
        archive.append(fs.createReadStream(pdf), {name: 'licence.pdf'});

        // pipe archive data to the file
        archive.pipe(output);
        console.log("FINIALIGZING");

        archive.finalize();


        console.log("Downloading : " + folderPath);
    });


    router.get("/clone/:username/:projectname", (req, res) => {

        var link_identifier = req.params.username + "/" + req.params.projectname;
        var projectName = req.params.projectname;
        const gitDir = sharedconsts.__gitDirName + '/';
        const gitTmps = sharedconsts.__gitTmps + '/';

        const folderPath = gitDir + link_identifier;
        var output = fs.createWriteStream(gitTmps + projectName + ".zip");

        console.log("OUTPUT : " + gitTmps + projectName + ".zip");

        var archive = archiver('zip', {
            zlib: {level: 9} // Sets the compression level.
        });

        output.on('close', function () {
            console.log(archive.pointer() + ' total bytes');
            console.log('archiver has been finalized and the output file descriptor has closed.');

            console.log("Downlodinth this : " + gitTmps + projectName + '.zip');
            res.download(gitTmps + projectName + '.zip');
        });

        output.on('end', function () {
            console.log('Data has been drained');
        });



        archive.on('warning', function (err) {
            if (err.code === 'ENOENT') {
                // log warning
            } else {
                // throw error
                throw err;
            }
        });

        // good practice to catch this error explicitly
        archive.on('error', function (err) {
            throw err;
        });

        console.log("ZIPPIN THIS : " + folderPath);


        archive.directory(folderPath, false);


        var pdf = sharedconsts.__gitDirName + '/licence.pdf';
        archive.append(fs.createReadStream(pdf), {name: 'licence.pdf'});

        // pipe archive data to the file
        archive.pipe(output);
        console.log("FINIALIGZING");

        archive.finalize();


        console.log("Downloading : " + folderPath);

        // zip archive of your folder is ready to download


        // this workds event with IDM
        //res.download('C:\\Users\\wildchild\\Desktop' + '/offers.zip');

        //output = 'C:\\xampp\\htdocs\\gittmps\\hello.zip' ;


    });

    router.get("/desktop/api/:username/:projectname", (req, res) => {
        var link_identifier = req.params.username + "/" + req.params.projectname;
        var projectName = req.params.projectname;


    });


    router.post('/test/tree', function (req, res) {
        const tree = fileUtils.fileSystemTreeFromPath(req.body.path);
        res.end(tree);
    });

    router.get('/test/find', function (req, res) {

        neode.findById('Person', 12)
            .then(person => {
                console.log(person.get('firstName')); // 1
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.end(JSON.stringify("PROJECT.JS IS ALVIE"));
            });
    });


    router.get('/users/:id', (req, res) => {

        neode.findById('Person', req.params.id)
            .then(res => {
                if (!res)
                    return "{}";
                return res.toJson();
            })
            .then(json => {
                res.send(json);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });
    })
    router.get('/api/userss/:id', (req, res) => {

        neode.cypher('match (n:Person) where ID(n) =  '+req.params.id+ 'return n.username')
            .then(res => {
                if (!res)
                    return "{}";
                return res.toJson();
            })
            .then(json => {
                res.send(json);
            })
            .catch(e => {
                res.status(500).send(e.stack);
            });
    });

    router.post('/api/membres/new', function (req, res) {

        neode.cypher("CREATE (ee:Member { name: '" + req.body.name + "', role: '" + req.body.role + "' })");

        /*neode.create("Project",req.body);*/
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(JSON.stringify(req.body));
    });


    router.get('/test/projectjs', function (req, res) {
        const dirs = fileUtils.getDirectoriesFromPath("C:\\xampp\\htdocs\\gitrepo\\admin\\Testingstars\\master")

        let recentDirName = new Date("1970-01-01"); //1970 unix
        let nameHolder = "";
        console.log("FORRRRRR");
        for (const d in dirs) {

            console.log(dirs[d]);
            const selectedDate = new Date(parseInt(dirs[d]) * 1000);

            console.log("Comparin those 2 : " + selectedDate + " / " + recentDirName);
            if (selectedDate > recentDirName) {
                recentDirName = selectedDate;
                console.log("Recent > Selected");
                nameHolder = dirs[d];
            }
        }

        console.log("Most recent one is : " + nameHolder);

        //root/hello/master/
        // ==> root/hello/newbranch/

        fileUtils.recursCopyFilesWithDirName("admin\\Testingstars\\master",
            "admin\\Testingstars\\NewBranchWork", nameHolder);

        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(JSON.stringify("PROJECT.JS IS ALVIE"));
    });


    return router;
}
;
