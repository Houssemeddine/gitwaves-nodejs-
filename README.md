# NODE JS GIT WAV SERVER

Based on a repository that contains an example project using the 
[Neode](http://github.com/adam-cowley/neode) package for NodeJS.
[NodeMon](https://github.com/remy/nodemon) package for Nodemon.

## Models

The basic models are defined in the `./models` directory.  
The models are all based on the Recommendations example in the [Neo4j Sandbox](https://neo4j.com/sandbox-v2).

### Running the server
Firstly, Check Settings inside .env file


***
```
npm install
nodemon server.js (check commands in git pages)
//node server.js
```
***

***
```
INSTALLATION : 
Clean NEO4J DB : match (n) detach delete n;

```
***

***
backend : http://41.226.11.252:6080/Derouiche.elyes/nodebackend
frontend : http://41.226.11.252:6080/Derouiche.elyes/gitwaves
***
