var sharedconsts = require('../sharedconsts');
var mkdirp = require('mkdirp');
var fs = require('fs');
var path = require('path');
var file = require('file-system');
const dirTree = require("directory-tree");
var extract = require('extract-zip');


var dict = []; // create an empty array

const gitDir = sharedconsts.__gitDirName + '/';

module.exports = {
    copyFile: function (pathFrom, pathTo) {
        console.log("COPY File  : " + pathFrom + " TO : " + pathTo);
    },

    recursCopyFiles: function (pathFrom, pathTo) {
        console.log("RECURS COPY Files  : " + pathFrom + " TO : " + pathTo);
        file.copySync(gitDir + pathFrom, gitDir + pathTo);
    },

    recursCopyFilesWithDirName: function (pathFrom, pathTo, dirName) {
        console.log("RECURS COPY Files  : " + pathFrom + " TO : " + pathTo);

        console.log("MKDIR  : " +gitDir + pathFrom +"/"+ dirName);
        file.mkdir(gitDir + pathTo + "/" + dirName);

       file.copySync(gitDir + pathFrom +"/"+ dirName,
           gitDir + pathTo + "/" + dirName);
    },

    getDirectoriesFromPath: function (path) {
        return fs.readdirSync(path).filter(function (file) {
            if (fs.statSync(path + '/' + file).isDirectory()) {
                // console.log(path + '/' + file);
                //console.log(file.split(path.sep).pop());
                return file.split(path.sep).pop();
            }
        });
    },

    getRecentDirectoryFromPath: function (path) {
        return fs.readdirSync(path).filter(function (file) {
            var recentDirName = new Date("1970-01-01"); //1970 unix
            if (fs.statSync(path + '/' + file).isDirectory()) {
                // console.log(path + '/' + file);
                console.log(file.split(path.sep).pop());
                var selectedDate = new Date(parseInt(file.split(path.sep).pop()) * 1000);

                console.log("Comparin those 2 : " + selectedDate + " / " + recentDirName);
                if (selectedDate > recentDirName) {
                    recentDirName = selectedDate;
                    console.log("Recent > Selected");
                }
                return recentDirName;
            }
        });
    },

    recursCopyRecentFiles: function (pathFrom, pathTo) {
        // Pick the recent file

        const dirs = getDirectoriesFromPath(pathFrom);

        for (const d in dirs) {
            console.log("Dir Name ");
            console.log(d);
        }

        console.log("RECURS COPY Files  : " + pathFrom + " TO : " + pathTo);
        //file.copySync(gitDir + pathFrom, gitDir + pathTo);
    },

    getDirUpdateTime: function (path) {
        const stats = fs.statSync(path);
        return stats.mtime
    },

    createDir: function (path) {
        console.log("Create DIR : " + gitDir + path);
        file.mkdir(gitDir + path, [], function (error) {
            console.log("Callback create dir");
        });
    },

    moveFile: function (pathFrom, pathTo) {
        console.log("Move File  : " + pathFrom + " TO : " + pathTo);
    },

    extractFilePathFromUrl: function (urlPath) {
        console.log("Extract URL FROM : " + urlPath);
    },

    fileSystemTreeFromPath: function (projectPath) {
        const filteredTree = dirTree(gitDir + projectPath);
        //console.log(JSON.stringify(filteredTree));
        return filteredTree;
    },

    extractZipFileWithPathTo: function (source, linkIdentifier) {

        var fullPath = gitDir + linkIdentifier;
        console.log("Will unzip here : " + fullPath);

        // repo should have been init , no need to create

        // Stage 1 : Unzip content
        extract(source, {dir: fullPath}, function (err) {
            // extraction is complete. make sure to handle the err
            console.log("SHit happeneed successfully");

        })

    }
};
