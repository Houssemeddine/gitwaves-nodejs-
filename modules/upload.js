var sharedconsts = require('../sharedconsts');
var mkdirp = require('mkdirp');
var fs = require('fs');
var path = require('path');
var fileUtils = require('../modules/GFileSystUtils');

const form = require('formidable').IncomingForm({
    uploadDir: sharedconsts.__dirname + '/',  // don't forget the __dirname here
    keepExtensions: true,
    keepFilename : true
});

var dict = []; // create an empty array


module.exports = function upload(req,res){
    //   var form = new IncomingForm();
    console.log("Called");

    console.log(req.params.filename);
    var link_identifier = req.params.filename;
    form.on('file', (field, file) => {
        // Do something with the fjile
        // e.g. save it to the database
        // you can access it using file.path
        console.log(file.path);

        // Lets proceed the content

        // Stage 1 :
        fileUtils.extractZipFileWithPathTo(file.path, link_identifier);
    });
    form.on('end', () => {
        res.json();
    });
    form.parse(req);
};


