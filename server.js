/**
 * A basic example using Express to create a simple movie recommendation engine.
 */
const express = require('express');
const session = require('express-session');
const path = require('path');
const bodyParser = require('body-parser');

const cors = require('cors');

var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200
};


/**
 * Load Neode with the variables stored in `.env` and tell neode to
 * look for models in the ./models directory.
 */
const neode = require('neode')
    .fromEnv()
    .withDirectory(path.join(__dirname, 'models'));

neode.setEnterprise(true);


/**
 * Create a new Express instaince
 */
const app = express();


/**
 * Tell express to use jade as the view engine and to look for views
 * inside the ./views folder
 */
app.set('view engine', 'jade');
app.set('views', path.join(__dirname, '/views'));

/**
 * SCRF for AJAX requests used in /recommend/:genre
 */
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

/**
 * Set up a simple Session
 */
app.use(session({
    genid: function () {
        return require('uuid').v4();
    },
    resave: false,
    saveUninitialized: true,
    secret: 'neoderocks'
}));

/**
 * Serve anything inside the ./public folder as a static resource
 */
app.use(express.static('public'));

/**
 * Display home page with a list of Genres
 app.get('/', (req, res) => {
    neode.all('Movie')
        .then(genres => {
            res.render('index', {title: 'Home', genres});
        });
});
 */

/**
 * For examples of how to use Neode to quickly generate a REST API,
 * checkout the route examples in ./routes.api.js
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(require('./routes/api')(neode));
app.use(require('./routes/project')(neode));
app.use(require('./routes/gitwaves')(neode));

app.use(cors(corsOptions));


/**
 * Listen for requests on port 3000
 */
app.listen(3000, function () {
    console.log('app listening on http://localhost:3000'); // eslint-disable-line no-console
});


